import React from 'react';
import PropTypes from 'prop-types';

import Box from './box';

/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/no-array-index-key */
export default class Board extends React.Component {
  render() {
    return (
      <div>
        {this.props.board.map((element, i) => (
          <div key={`row_${i}`}>
            {element.map((box, j) => (
              <Box
                v={i}
                h={j}
                key={`${i},${j}`}
                value={box}
                winner={this.props.winner}
                onClick={this.props.onClick}
              />
            ))}
          </div>
        ))}
      </div>
    );
  }
}

Board.propTypes = {
  board: PropTypes.array,
  winner: PropTypes.string,
  onClick: PropTypes.func,
};
