import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';

/* eslint-disable react/prefer-stateless-function */
export default class Box extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: '#D3D3D3',
    };
  }

  componentWillReceiveProps(prevProps) {
    if (this.props.value !== prevProps.value) {
      if (prevProps.value === 'X') {
        this.setState({
          backgroundColor: '#EE3233',
        });
      } else if (prevProps.value === 'O') {
        this.setState({
          backgroundColor: '#66A7C5',
        });
      } else {
        this.setState({
          backgroundColor: '#D3D3D3',
        });
      }
    }
  }

  render() {
    const buttonStyle = {
      width: '100px',
      height: '100px',
      border: '1px solid black',
      display: 'inline',
      backgroundColor: this.state.backgroundColor,
    };

    return (
      <Button
        v={this.props.v}
        h={this.props.h}
        style={buttonStyle}
        disabled={this.props.value !== '?' || this.props.winner !== null}
        onClick={() => this.props.onClick(this.props.v, this.props.h)}
      >
        {this.props.value}
      </Button>
    );
  }
}

Box.propTypes = {
  v: PropTypes.number,
  h: PropTypes.number,
  value: PropTypes.string,
  onClick: PropTypes.func,
  winner: PropTypes.string,
};
