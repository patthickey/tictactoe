import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Chip, Button } from '@material-ui/core';

import Board from '../../components/Board';

/* eslint-disable react/prefer-stateless-function */
/* eslint no-plusplus: ["error", { "allowForLoopAfterthoughts": true }] */
export default class CustomGame extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = this.initialState;
    this.setValue = this.setValue.bind(this);
    this.resetGame = this.resetGame.bind(this);
  }

  get initialState() {
    const table = [];
    for (let i = 0; i < this.props.size; i++) {
      const row = [];
      for (let j = 0; j < this.props.size; j++) {
        row.push('?');
      }
      table.push(row);
    }

    return {
      size: this.props.size,
      toWin: this.props.toWin,
      turn: 0,
      winner: null,
      board: table,
    };
  }

  resetGame() {
    this.setState(this.initialState);
  }

  setValue(v, h) {
    const newBoard = this.state.board.slice();
    const turnCount = this.state.turn + 1;
    if (this.state.turn % 2 === 0) {
      newBoard[v][h] = 'X';
    } else {
      newBoard[v][h] = 'O';
    }
    const winner = this.checkWinner(newBoard, v, h);
    this.setState({
      turn: turnCount,
      board: newBoard,
      winner,
    });
  }

  checkWinner(board, v, h) {
    let min;
    let max;
    let current;
    let inARow;
    // vertical
    min = v - this.state.toWin + 1;
    if (min <= 0) min = 0;
    max = v + this.state.toWin + -1;
    if (max >= this.state.size) max = this.state.size - 1;
    current = '';
    inARow = 0;
    // console.log(`v ${v}, h ${h}`);
    // console.log(`min ${min}, max ${max}`);
    for (let i = min; i <= max; i++) {
      if (board[i][h] !== '?') {
        if (board[i][h] !== current) {
          current = board[i][h];
          inARow = 1;
        } else {
          inARow += 1;
        }
      }
      // console.log(`In A Row: ${inARow}`);
      if (inARow === this.state.toWin) {
        return board[i][h];
      }
    }

    // horizontal
    min = h - this.state.toWin + 1;
    if (min <= 0) min = 0;
    max = h + this.state.toWin + -1;
    if (max >= this.state.size) max = this.state.size - 1;
    current = '';
    inARow = 0;
    // console.log(`v ${v}, h ${h}`);
    // console.log(`min ${min}, max ${max}`);
    for (let i = min; i <= max; i++) {
      if (board[v][i] !== '?') {
        if (board[v][i] !== current) {
          current = board[v][i];
          inARow = 1;
        } else {
          inARow += 1;
        }
      }
      // console.log(`In A Row: ${inARow}`);
      if (inARow === this.state.toWin) {
        return board[v][i];
      }
    }

    let vMin;
    let vMax;
    let hMin;
    let hMax;

    // diagonal down
    vMin = v - this.state.toWin + 1;
    vMax = v + this.state.toWin + -1;
    hMin = h - this.state.toWin + 1;
    hMax = h + this.state.toWin + -1;
    while (vMin < 0 || hMin < 0) {
      vMin += 1;
      hMin += 1;
    }
    while (vMax >= this.state.size || hMax >= this.state.size) {
      vMax -= 1;
      hMax -= 1;
    }

    current = '';
    inARow = 0;
    // console.log(`v ${v}, h ${h}`);
    // console.log(`vMin ${vMin}, hMin ${hMin}`);
    // console.log(`vMax ${vMax}, hMax ${hMax}`);
    for (let i = vMin; i <= vMax; i++) {
      if (hMin <= hMax) {
        if (board[i][hMin] !== '?') {
          if (board[i][hMin] !== current) {
            current = board[i][hMin];
            inARow = 1;
          } else {
            inARow += 1;
          }
        }
        if (inARow === this.state.toWin) {
          return board[i][hMin];
        }
        hMin += 1;
      }
    }

    // diagonal up
    vMax = v - this.state.toWin + 1;
    vMin = v + this.state.toWin + -1;
    hMin = h - this.state.toWin + 1;
    hMax = h + this.state.toWin + -1;

    while (vMin >= this.state.size || hMin < 0) {
      vMin -= 1;
      hMin += 1;
    }
    while (hMax >= this.state.size || vMax < 0) {
      hMax -= 1;
      vMax += 1;
    }
    current = '';
    inARow = 0;
    // console.log(`v ${v}, h ${h}`);
    // console.log(`vMin ${vMin}, hMin ${hMin}`);
    // console.log(`vMax ${vMax}, hMax ${hMax}`);

    for (let i = hMin; i <= hMax; i++) {
      if (vMin >= vMax) {
        if (board[vMin][i] !== '?') {
          if (board[vMin][i] !== current) {
            current = board[vMin][i];
            inARow = 1;
          } else {
            inARow += 1;
          }
        }
        if (inARow === this.state.toWin) {
          return board[vMin][i];
        }
        vMin -= 1;
      }
    }
    return null;
  }

  render() {
    let winner;
    if (this.state.winner) {
      winner = (
        <Chip
          style={{
            backgroundColor: this.state.winner === 'X' ? '#EE3233' : '#66A7C5',
          }}
          label={`Winner: ${this.state.winner}`}
        />
      );
    }
    return (
      <div>
        <Board
          board={this.state.board}
          winner={this.state.winner}
          onClick={this.setValue}
        />
        <Link to="/">
          <Button variant="outlined" color="primary">
            Home
          </Button>
        </Link>
        <Button
          type="submit"
          color="primary"
          variant="raised"
          onClick={this.resetGame}
        >
          reset game
        </Button>
        {winner}
      </div>
    );
  }
}

CustomGame.propTypes = {
  size: PropTypes.number,
  toWin: PropTypes.number,
};
