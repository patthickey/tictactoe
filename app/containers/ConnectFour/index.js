import React from 'react';
import { Link } from 'react-router-dom';
import { Chip, Button } from '@material-ui/core';

import Board from '../../components/Board';

/* eslint-disable react/prefer-stateless-function */
/* eslint no-plusplus: ["error", { "allowForLoopAfterthoughts": true }] */
export default class ConnectFour extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = this.initialState;
    this.setValue = this.setValue.bind(this);
    this.resetGame = this.resetGame.bind(this);
  }

  get initialState() {
    const table = [];
    for (let i = 0; i < 9; i++) {
      const row = [];
      if (i === 8) {
        for (let j = 0; j < 9; j++) {
          row.push('?');
        }
      } else {
        for (let j = 0; j < 9; j++) {
          row.push('');
        }
      }
      table.push(row);
    }

    return {
      size: 9,
      turn: 0,
      winner: null,
      board: table,
    };
  }

  resetGame() {
    this.setState(this.initialState);
  }

  setValue(v, h) {
    const newBoard = this.state.board.slice();
    const turnCount = this.state.turn + 1;
    if (this.state.turn % 2 === 0) {
      newBoard[v][h] = 'X';
    } else {
      newBoard[v][h] = 'O';
    }
    if (v > 0) {
      newBoard[v - 1][h] = '?';
    }
    const winner = this.checkWinner(newBoard);
    this.setState({
      turn: turnCount,
      board: newBoard,
      winner,
    });
  }

  checkWinner(board) {
    for (let v = 0; v < this.state.size; v++) {
      for (let h = 0; h < this.state.size; h++) {
        if (board[v][h] !== '?' && board[v][h] !== '') {
          // horizontal
          if (
            h <= this.state.size - 4 &&
            board[v][h] === board[v][h + 1] &&
            board[v][h] === board[v][h + 2] &&
            board[v][h] === board[v][h + 3]
          ) {
            return board[v][h];
          }
          // vertical
          if (
            v <= this.state.size - 4 &&
            board[v][h] === board[v + 1][h] &&
            board[v][h] === board[v + 2][h] &&
            board[v][h] === board[v + 3][h]
          ) {
            return board[v][h];
          }
          // diagonal right
          if (
            v <= this.state.size - 4 &&
            h <= this.state.size - 4 &&
            board[v][h] === board[v + 1][h + 1] &&
            board[v][h] === board[v + 2][h + 2] &&
            board[v][h] === board[v + 3][h + 3]
          ) {
            return board[v][h];
          }
          // diagonal left
          if (
            v <= this.state.size - 4 &&
            h >= 3 &&
            board[v][h] === board[v + 1][h - 1] &&
            board[v][h] === board[v + 2][h - 2] &&
            board[v][h] === board[v + 3][h - 3]
          ) {
            return board[v][h];
          }
        }
      }
    }
    return null;
  }

  render() {
    let winner;
    if (this.state.winner) {
      winner = (
        <Chip
          style={{
            backgroundColor: this.state.winner === 'X' ? '#EE3233' : '#66A7C5',
          }}
          label={`Winner: ${this.state.winner}`}
        />
      );
    }
    return (
      <div>
        <Board
          board={this.state.board}
          winner={this.state.winner}
          onClick={this.setValue}
        />
        <Link to="/">
          <Button variant="outlined" color="primary">
            Home
          </Button>
        </Link>
        <Button
          type="submit"
          color="primary"
          variant="raised"
          onClick={this.resetGame}
        >
          reset game
        </Button>
        {winner}
      </div>
    );
  }
}
