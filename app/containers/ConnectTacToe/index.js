import React from 'react';
import { Link } from 'react-router-dom';
import { TextField, InputLabel, Button, Chip } from '@material-ui/core';

import CustomGame from '../../components/CustomGame';

/* eslint-disable react/prefer-stateless-function */
export default class ConnectTacToe extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = this.initialState;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.resetSettings = this.resetSettings.bind(this);
  }

  get initialState() {
    return {
      size: 0,
      toWin: 0,
      error: 'Select size and in a row',
    };
  }

  resetSettings() {
    this.setState(this.initialState);
  }

  handleChange({ target }) {
    value = parseInt(target.value, 10);
    let value;
    if (target.value === '') {
      value = 0;
    } else {
      value = parseInt(target.value, 10);
    }

    this.setState({
      [target.id]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const error = this.validate();
    this.setState({
      error,
    });
  }

  validate() {
    if (this.state.size === 0 && this.state.toWin === 0) {
      return 'Select size and in a row';
    } else if (this.state.size < 3 || this.state.size > 10) {
      return 'Size must be between >=3 or <= 10';
    } else if (this.state.toWin < 3 || this.state.toWin > this.state.size) {
      return 'To Win must be between >=3 or <= Size';
    }
    return '';
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <InputLabel htmlFor="size">
            Size:
            <TextField
              id="size"
              type="number"
              value={this.state.size}
              onChange={this.handleChange}
              disabled={this.state.error === ''}
            />
          </InputLabel>
          <br />
          <InputLabel htmlFor="toWin">
            In A Row To Win:
            <TextField
              id="toWin"
              type="number"
              value={this.state.toWin}
              onChange={this.handleChange}
              disabled={this.state.error === ''}
            />
          </InputLabel>
          <br />
          <Button
            type="submit"
            color="primary"
            variant="raised"
            disabled={this.state.error === ''}
          >
            Submit
          </Button>
          <Button
            type="submit"
            color="primary"
            variant="raised"
            onClick={this.resetSettings}
          >
            Reset
          </Button>
          <Link to="/">
            <Button variant="outlined" color="primary">
              Home
            </Button>
          </Link>
          {this.state.error !== '' && <Chip label={this.state.error} />}
        </form>
        {this.state.error === '' && (
          <CustomGame size={this.state.size} toWin={this.state.toWin} />
        )}
      </div>
    );
  }
}
