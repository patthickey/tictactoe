import React from 'react';
import { Link } from 'react-router-dom';
import { Chip, Button } from '@material-ui/core';

import Board from '../../components/Board';

/* eslint-disable react/prefer-stateless-function */
/* eslint no-plusplus: ["error", { "allowForLoopAfterthoughts": true }] */
export default class TicTacToe extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = this.initialState;
    this.setValue = this.setValue.bind(this);
    this.resetGame = this.resetGame.bind(this);
  }

  get initialState() {
    const table = [];
    for (let i = 0; i < 3; i++) {
      const row = [];
      for (let j = 0; j < 3; j++) {
        row.push('?');
      }
      table.push(row);
    }

    return {
      size: 3,
      turn: 0,
      winner: null,
      board: table,
    };
  }

  resetGame() {
    this.setState(this.initialState);
  }

  setValue(v, h) {
    const newBoard = this.state.board.slice();
    const turnCount = this.state.turn + 1;
    if (this.state.turn % 2 === 0) {
      newBoard[v][h] = 'X';
    } else {
      newBoard[v][h] = 'O';
    }
    const winner = this.checkWinner(newBoard);
    this.setState({
      turn: turnCount,
      board: newBoard,
      winner,
    });
  }

  checkWinner(board) {
    for (let i = 0; i < this.state.size; i++) {
      if (
        board[i][0] !== '?' &&
        board[i][0] === board[i][1] &&
        board[i][1] === board[i][2]
      ) {
        return board[i][0];
      } else if (
        board[0][i] !== '?' &&
        board[0][i] === board[1][i] &&
        board[1][i] === board[2][i]
      ) {
        return board[0][i];
      }
    }
    if (
      board[0][0] !== '?' &&
      board[0][0] === board[1][1] &&
      board[1][1] === board[2][2]
    ) {
      return board[0][0];
    } else if (
      board[0][2] !== '?' &&
      board[0][2] === board[1][1] &&
      board[1][1] === board[0][2]
    ) {
      return board[0][2];
    }
    return null;
  }

  render() {
    let winner;
    if (this.state.winner) {
      winner = (
        <Chip
          style={{
            backgroundColor: this.state.winner === 'X' ? '#EE3233' : '#66A7C5',
          }}
          label={`Winner: ${this.state.winner}`}
        />
      );
    }
    return (
      <div>
        <Board
          board={this.state.board}
          winner={this.state.winner}
          onClick={this.setValue}
        />
        <Link to="/">
          <Button variant="outlined" color="primary">
            Home
          </Button>
        </Link>
        <Button
          type="submit"
          color="primary"
          variant="raised"
          onClick={this.resetGame}
        >
          reset game
        </Button>
        {winner}
      </div>
    );
  }
}
