/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Grid } from '@material-ui/core';

import HomePage from 'containers/HomePage/Loadable';
import TicTacToe from 'containers/TicTacToe';
import ConnectFour from 'containers/ConnectFour';
import ConnectTacToe from 'containers/ConnectTacToe';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

export default function App() {
  return (
    <Grid container justify="center" alignItems="center">
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/TicTacToe" component={TicTacToe} />
        <Route exact path="/ConnectFour" component={ConnectFour} />
        <Route exact path="/ConnectTacToe" component={ConnectTacToe} />
        <Route component={NotFoundPage} />
      </Switch>
    </Grid>
  );
}
