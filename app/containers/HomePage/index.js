/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Link } from 'react-router-dom';
import {
  TextField,
  InputLabel,
  Button,
  Chip,
  MenuItem,
} from '@material-ui/core';

const player01Colors = [
  {
    value: 'blue',
    label: 'blue',
  },
  {
    value: 'red',
    label: 'red',
  },
  {
    value: 'yellow',
    label: 'yellow',
  },
];

const player02Colors = [
  {
    value: 'purple',
    label: 'purple',
  },
  {
    value: 'orange',
    label: 'orange',
  },
  {
    value: 'green',
    label: 'green',
  },
];

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = this.initialState;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.resetSettings = this.resetSettings.bind(this);
  }

  get initialState() {
    return {
      player01Name: '',
      player01Color: 'blue',
      player02Name: '',
      player02Color: 'purple',
      error: 'Set player names please',
    };
  }

  resetSettings() {
    this.setState(this.initialState);
  }

  handleChange({ target }) {
    const location = target.id ? target.id : target.name;
    this.setState({
      [location]: target.value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const error = this.validate();
    this.setState({
      error,
    });
  }

  validate() {
    if (this.state.player01Name === '' && this.state.player02Name === '') {
      return 'Set player names please';
    } else if (this.state.player01Name === '') {
      return 'Select a name for player 01';
    } else if (this.state.player02Name === '') {
      return 'Select a name for player 02';
    }
    return '';
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <InputLabel htmlFor="size">
            Player One:
            <TextField
              id="player01Name"
              type="string"
              value={this.state.player01Name}
              onChange={this.handleChange}
              disabled={this.state.error === ''}
            />
          </InputLabel>
          <br />
          <InputLabel htmlFor="size">
            Player One Color:
            <TextField
              select
              name="player01Color"
              value={this.state.player01Color}
              onChange={this.handleChange}
            >
              {player01Colors.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </InputLabel>
          <br />
          <InputLabel htmlFor="toWin">
            Player Two:
            <TextField
              id="player02Name"
              type="string"
              value={this.state.player02Name}
              onChange={this.handleChange}
              disabled={this.state.error === ''}
            />
          </InputLabel>
          <br />
          <InputLabel htmlFor="size">
            Player Two Color:
            <TextField
              select
              name="player02Color"
              value={this.state.player02Color}
              onChange={this.handleChange}
            >
              {player02Colors.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </InputLabel>
          <br />
          <Button
            type="submit"
            color="primary"
            variant="raised"
            disabled={this.state.error === ''}
          >
            Submit
          </Button>
          <Button
            type="submit"
            color="primary"
            variant="raised"
            onClick={this.resetSettings}
          >
            Reset
          </Button>
          {this.state.error !== '' && <Chip label={this.state.error} />}
        </form>
        {this.state.error === '' && (
          <div>
            <Link to="/TicTacToe">
              <Button variant="outlined" color="primary">
                Tic Tac Toe
              </Button>
            </Link>
            <Link to="/ConnectFour">
              <Button variant="outlined" color="primary">
                Connect Four
              </Button>
            </Link>
            <Link to="/ConnectTacToe">
              <Button variant="outlined" color="primary">
                Connect Tac Toe
              </Button>
            </Link>
          </div>
        )}
      </div>
    );
  }
}
