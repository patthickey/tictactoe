A quick little game application using React Boilerplate as a base.

Working games:

* Tic Tac Toe
* Connect Four
* Connect Tac Toe (looks like Connect Four, plays like Tic Tac Toe)
  * Has customizable sizing and win rules
  * Has validation

Coming soon:

* Custom user naming / color choosing
  * Setting is there, but it currently does nothing





## License

This project is licensed under the MIT license, Copyright (c) 2018 Maximilian
Stoiber. For more information see `LICENSE.md`.
